package com.simplePage;

public class SimplePage {

    public void displayDetails(String name,int age , String sex){
        System.out.println("Name "+name);
        System.out.println("Age "+age);
        System.out.println("Sex "+sex);
    }

    public String validateage(int age){
        String allowed=null;
        if(age>=18){
            allowed="Yes";
        }
        else{
            allowed="No";
        }
        return allowed;
    }

    public String maleCandidate(String sex){
        String male=null;
        if(sex.equalsIgnoreCase("Male")){
            male="Yes";
        }
        else{
            male="No";
        }
        return male;
    }

    public String femaleCandidate(String sex){
        String female=null;
        if(sex.equalsIgnoreCase("Female")){
            female="Yes";
        }
        else{
            female="No";
        }
        return female;
    }
}
