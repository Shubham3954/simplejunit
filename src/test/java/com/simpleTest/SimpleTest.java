package com.simpleTest;

import com.simplePage.SimplePage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

public class SimpleTest {

   static SimplePage simplePage = new SimplePage();
    private static String name= "Shubham";
    private static int age =16;
    private static String sex="Female";



    @BeforeAll
    public static void display(){
        simplePage.displayDetails(name,age,sex);
    }

    @Test
    @Order(1)
    public void ageValidation(){

        String value=simplePage.validateage(age);
        Assertions.assertEquals("Yes",value,"No you are not allowed to proceed");
    }

    @Test
    @Order(2)
    public void maleValidation(){

        String value=simplePage.maleCandidate(sex);
        Assertions.assertEquals("Yes",value,"No you are not allowed to proceed as male candidate");
    }

    @Test
    @Order(3)
    public void femaleValidation(){

        String value=simplePage.femaleCandidate(sex);
        Assertions.assertEquals("Yes",value,"No you are not allowed to proceed as female candidate");

    }
}
